/**
 * @file main.cpp
*/
#include <stdio.h>
#include <stdlib.h>
#include <iostream> //for the use of input and ouput stream

#include "../headers/solutions.h"


//to modify files
#include <fstream> 

using namespace std;


/**
 * @brief Function that write the result of the matrix in a CSV file
 *
 * @param fileName name of the file created (without the extension)
 * @param z table list with the z values
 * @param matrix matrix with the result to print in the file
 * @param row row to read to obtain the values
 * @return nothing
 */

// todo : you might need to write std::cout and std::endl instead of cout and endl (maybe the line "using namespace constants;" is enough)
void valueInCsv(string fileName, arma::rowvec z, arma::mat matrix, int row, int nb)
{
 
    string const myFile("./filesResult/" + fileName + ".csv"); //creation of the file in the folder filesResult that contains all files result
    ofstream myFlux(myFile.c_str()); //The file is open

    //If the file is correctly open
    if(myFlux)
    {
        for (int i = 0; i < nb; i++)
        {
            myFlux << z(i) << "," << matrix(row, i) << endl; //We write the result in a new line of the file
        }
    }
    //If the file isn't correctly opened, we print an error message
    else
    {
        cout << "ERROR : cannot open the file" << endl;
    }
    myFlux.close();//The file is closed
}





int main()
{
    //Initialisation of the variables
    int nb = constants::nb_points; //nb points required
    int mode; //chosen mode (easy, hard)
    int system; //chosen system (electron, neutron)

    //Initialisation of the parameters needeed for the methods
    double m;
    double w;
    double h_bar;
    double min; //distance min in meter
    double max; //distance max in meter

    //The user choose a mode
    cout << "Choose a mode :" << endl;
    cout << " 1 : easy mode" << endl;
    cout << " 2 : hard mode" << endl;
    cin >> mode; //We stock the mode chosen by the user

    while(mode < 0 || mode > 2)
    {
        cout << "error: invalid input. Please enter an integer between 1 and 3\n";
        cout << "Choose a mode :" << endl;
        cout << " 1 : easy mode" << endl;
        cout << " 2 : hard mode" << endl;
        cin >> mode; //We stock the mode chosen by the user
    }

    //We define the values according to the chosen mode
    if (mode == 1)
    {
        m = constants::m_easy;
        w = constants::w_easy;
        h_bar = constants::hbar_easy;
        min = -5.; 
        max = 5.; 
    }
    else //mode == 2
    {
        cout << "Choose the system :" << endl;
        cout << " 1 : neutron in a nuclear potential" <<endl;
        cout << " 2 : electron in an atomic potential" <<endl;
        cin >> system;

        while(system < 0 || system > 2)
        {
            cout << "error: invalid input. Please enter an integer between 1 and 3\n";
            cout << "Choose the system :" << endl;
            cout << " 1 : neutron in a nuclear potential" <<endl;
            cout << " 2 : electron in an atomic potential" <<endl;
            cin >> system;
        }

        if (system == 1)
        {
            m = constants::m_n;
            w = constants::w_n;
            min = -6e-10;
            max = 6e-10;
        }
        else //system == 2
        {
            m = constants::m_e;
            w = constants::w_e;
            min = -3e-8;
            max = 3e-8;
        }

        h_bar = constants::hbar; //does not depend of the system
    }





    /* ------ Resolution of the solutions ------ */

    //We ask the user how many solution they want (constants::nb_points by default)
    cout << "How many points do you want ? (lots of points is recommended ~ <2000)" << endl;
    cin >> nb;

    //We calculate the solutions
    Solutions M = Solutions(constants::nb_level, nb);
    arma::rowvec z;
    z = M.Solutions::set_mesh(min, max, nb);
    M.Solutions::generate_psi(m, w, h_bar, z);


    string fileName; //name of the files

    /*
        We write the values of each row of the matrix (each row correspond to a level of energy) in a file
    */
    for (int n = 0; n < constants::nb_level; n++) //i is the level of energy
    {
        fileName = "level_" + to_string(n); //We define the name of the file (one per level of energy), the file name will have the form : level_nb (level_1, level_2, ...)
        valueInCsv(fileName, z, M.psi_matrix, n, nb); //We write the points in a CSV file
    }

    return 0;
}