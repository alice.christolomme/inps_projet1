import matplotlib.pyplot as plt #for print function

hbar = 1
w =1

def read_file(file, n):
    file_o = open('./filesResult/' + file +'.csv', "r") #We open the file in reading mode
    file_r = file_o.readlines()  #We read the lines of the file

    l=[] #creation of an empty list to stock the informations of the file
    #For each line, the information are stock in the l list
    for i in file_r:
        l.append(i)

    #Initialisation of the 2 vectors we will used to plot
    x,y = [], []
    '''For each element of l list, we split the information according to the split define (here ',')
    and we stock the information in our 2 variables define previously'''
    for i in range(len(l)):
        l[i]=l[i].split(',')
        x.append(float(l[i][0])) #We convert the string value into a float/double one and add it to the vector
        y.append(float(l[i][1]) + n)
    return x,y

#We set the x-axis range
#plt.xlim(-5, 5) #for easy mode
plt.xlim(-6e-10, 6e-10) #for hard mode, neutron
#plt.xlim(-3e-8, 3e-8) #for hard mode, electron

x,y = [], []

x=[0 for i in range(8)]
y=[1.5, 3, 4.5, 6, 7.5, 9, 10.5, 12]
plt.plot(x,y, "w")

my_yticks = ['E0','E1','E2','E3','E4','E5','E6','E7']
plt.yticks(y, my_yticks)

delta = hbar * w / 2
x,y = read_file("level_0", delta)
plt.plot(x,y, label='psi_0(x)')
delta += hbar * w / 2
x,y = read_file("level_1", delta)
plt.plot(x,y, label='psi_1(x)')
delta += hbar * w / 2
x,y = read_file("level_2", delta)
plt.plot(x,y, label='psi_2(x)') ##ELLE EST CHELOU
delta += hbar * w / 2
x,y = read_file("level_3", delta)
plt.plot(x,y, label='psi_3(x)')
delta += hbar * w / 2
x,y = read_file("level_4", delta)
plt.plot(x,y, label='psi_4(x)')
delta += hbar * w / 2
x,y = read_file("level_5", delta)
plt.plot(x,y, label='psi_5(x)')
delta += hbar * w / 2
x,y = read_file("level_6", delta)
plt.plot(x,y, label='psi_6(x)')
delta += hbar * w / 2
x,y = read_file("level_7", delta)
plt.plot(x,y, label='psi_7(x)')




plt.title("Résultat du calcul") #title of the graph
plt.xlabel("Distance (m)") #name of the x-axis
#plt.ylabel("Energy (SI unit)") #name of the y-axis
plt.legend(loc=1)

plt.show() #print the graph