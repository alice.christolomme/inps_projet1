/** 
* \file solutions.cpp
* This file contains the declaration of the solutions class
*/

#include <math.h>

#include "../headers/solutions.h"

/** 
 * \brief Class of matrices that contain the solutions.
 * \param row number of row
 * \param col number of column
*/
Solutions::Solutions(int row, int col)
{
    psi_matrix.arma::mat::ones(row, col);
    H.arma::mat::ones(row, col);
    E.arma::rowvec::ones(col);
    F.arma::colvec::ones(row);
    scal.arma::mat::ones(row,row);
}

/**
 * @brief Fonction that calculate n factorial
 * 
 * @param n the factorial
 * @return n!
*/
int Solutions::fact(int n)
{
    int f = 1;
    for (int i = 1; i <= n; i++)
    {
        f *= i;
    }
    return f;
}


/**
 * @brief Fonction that calculate the points where we will evaluate the level of energy
 *
 * @param min the minimum of the interval
 * @param max the maximum of the interval
 * @param nb nb of points in the interval
 * 
 * @return a row vector that contains all the points where the solution will be evaluate
 */

arma::rowvec Solutions::set_mesh(double min, double max, int nb)
{
    return arma::linspace(min, max, nb).arma::mat::t();
}

/**
 * @brief Fonction that evaluates the Hermit matrix -> We want to evaluate the matrix at points sqrt(m*w/hbarre)*z
 *
 * @assigns the value of the Hermit matrix to H (attribute of the class)
 * @attention requires that the constructor has been called to set the size of the hermit matix : H .
 * 
 * @param z the vector with the points where we evaluate the function
 * 
 * @return nothing
 */
void Solutions::set_hermit(arma::rowvec z)
{
    //H_1(z)
    H.row(1) = 2 * H.row(1) % z;

    //H_{n+1}(z)
    for (int n = 1; n < constants::nb_level - 1; n++)  //n will reach nb_level - 2 which means we wil have nb_level of energy, up the (nb_level -1)th 
    {
        H.row(n+1) = 2 * z % H.row(n) - 2 * n * H.row(n-1);
    }
}


/**
 * @brief Fonction that assign the value of the vector exp needed to the attribute E
 *
 * @attention requires that the mesh method has already been called
 * 
 * @param m mass (depends of the mode and system chosen by the user)
 * @param w omega (depends of the mode and system chosen by the user)
 * @param hb habr (depends of the mode chosen by the user)
 * @param z the vector with the points where we evaluate the function
 * 
 * @return nothing
 */
void Solutions::set_vec_exp(double m, double w, double hb, arma::rowvec z)
{
    E = -1. * (m * w / (2 * hb)) * z % z;
    E = arma::exp(E);
}
/**
* @brief Fonction that set a vector needed for the calculation of psi, it is the factor depending on n (exception taken on the hermit factor)
*
* This function has no argument for only the maximal n is required and the Solutions::F has already be declared in the Constructor and we can therefore infer it from the size of F
*/
void Solutions::set_vec_F(){
    for ( int n = 1; n < F.n_rows; n++)
    {
        F(n)=sqrt( 1 / (fact(n) * pow(2,n)));
    }
}

/**
 * @brief Fonction that set the matrix psi, solutions of our problem
 *
 * @param m mass (depends of the mode and system chosen by the user)
 * @param w omega (depends of the mode and system chosen by the user)
 * @param hb h_bar (depends of the mode chosen by the user)
 * @param z the vector with the points where we evaluate the function
 * 
 * @return nothing
 */
void Solutions::generate_psi(double m, double w, double hb, arma::rowvec z)
{   
    double cst = sqrt(m*w/hb); //We evaluate the polynomials at points cst*z
    //new Z = cst * z (because we evaluate the polynomial at point cst*z)
    arma::rowvec z2 = cst * z;
    set_hermit(z2);  
    set_vec_exp(m, w, hb, z); 
    set_vec_F();
    //the constant part of the solution
    double C = m * w / (arma::datum::pi * hb);
    C = pow(C, 0.25);

    psi_matrix= C * E % H.each_row() ;
    psi_matrix= psi_matrix.each_col() % F;
}

/**
*@brief this function computes the scalar product beetween each pairs of psi solutions and put the result in the scal matrix
*
*@attention set_vec_F should have already been called
*the weights and points vectors have been obtain by running on python:
*numpy.polynomial.hermite.hermgauss((n*n)//2+1)
*where n is the number of level of energy
*
*for n=8: 
*arma::rowvec points={-7.25385182, -6.54165545, -5.94807118, -5.414929  , -4.92002852,-4.45191115, -4.00367161, -3.57072198, -3.14979668, -2.73844582, -2.33475115, -1.93715458, -1.54434826, -1.1552002 , -0.76870138, -0.38392601,  0.        ,  0.38392601,  0.76870138,  1.1552002 , 1.54434826,  1.93715458,  2.33475115,  2.73844582,  3.14979668, 3.57072198,  4.00367161,  4.45191115,  4.92002852,  5.414929  , 5.94807118,  6.54165545,  7.25385182};
*arma::rowvec weights={1.15331622e-23, 1.65709474e-19, 2.40778568e-16, 9.43481416e-14, 1.47398094e-11, 1.12892225e-09, 4.80774568e-08, 1.23769337e-06, 2.04236841e-05, 2.25442771e-04, 1.71845464e-03, 9.26568997e-03, 3.59879823e-02, 1.02069080e-01, 2.13493931e-01, 3.31552001e-01, 3.83785267e-01, 3.31552001e-01, 2.13493931e-01, 1.02069080e-01, 3.59879823e-02, 9.26568997e-03, 1.71845464e-03, 2.25442771e-04, 2.04236841e-05, 1.23769337e-06, 4.80774568e-08, 1.12892225e-09, 1.47398094e-11, 9.43481416e-14, 2.40778568e-16, 1.65709474e-19, 1.15331622e-23};
*
*@param points a vector of the points on which the hermit polynomials sould be evaluated to use quadrature rule
*@param weights a vector of the  weigths matching the points vector
*/

void Solutions::set_scal(arma::rowvec points, arma::rowvec weights)
{
    double cst=1/sqrt(arma::datum::pi);
    double sum;
    int l=points.n_cols;
    set_hermit(points);
    set_vec_F();
    for (int i=0; i<scal.n_rows; i++)
    {
        for (int j=0; j<i+1; j++) // the calculus is symetrical beetween i and j
        {
            sum=arma::accu(weights % H.row(i) % H.row(j));
            scal(i,j)=F(i)*F(j)*cst*sum;
            scal(j,i)=scal(i,j);
        }
    }
}