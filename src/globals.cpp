/**
*@file globals.cpp
*This file contains the declaration of all the global constants.

*There are two modes: simple mode and difficult mode. In simple mode, all the constants are initialised at 1. 
*In difficult mode, the constants are in SI units.
*/

#include <armadillo>
#include "../headers/globals.h"




namespace constants
{   
    /**
     * number of level of energy
    */
    int nb_level = 8;
        /**
     * number of level of evolution
    */
    int nb_points = 2001; 



    /* --------------------------- Constants for the easy mode --------------------------- */

    /**
    * hbar equals to h/(2*pi) with h the Plank's constant
    */
    double hbar_easy = 1.; // h/(2*pi)

    /**
    *w is the pulsation of the harmonic oscillator
    */
    double w_easy = 1.;

    /**
    m is the mass
    */
    double m_easy = 1.;



    /* --------------------------- Constants for the hard mode --------------------------- */

    /**
     * h is Plank's constant
    */
    double h = 6.62607015e-34; // in J.s
    /**
    * hbar equals to h/(2*pi) with h the Plank's constant
    */
    double hbar = h / (2*arma::datum::pi); // h/(2*pi) in J.s

    /**
     * c is the celery of light
    */
    double c = 2.99792458e8;

    /**
     * omega
    */
    double w_n = 5.091666e12; // v= 611 m.s-1
    double w_e = 4.1666e12; // v = 500 m.s-1

    /**
     * m_e is the mass of an electron, in kg
     * m_n is the mass of a neutron, in kg
     */

    double m_e = 9.1094e-31; 
    double m_n =  1.6749e-27;
};