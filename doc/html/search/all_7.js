var searchData=
[
  ['set_5fhermit_13',['set_hermit',['../classSolutions.html#a4712177198f4a74b0812a7682150896b',1,'Solutions']]],
  ['set_5fmesh_14',['set_mesh',['../classSolutions.html#a3699c88c49cf9cc13031a8670ada075f',1,'Solutions']]],
  ['set_5fscal_15',['set_scal',['../classSolutions.html#af7acfe82ad2f0091485286563322b56a',1,'Solutions']]],
  ['set_5fvec_5fexp_16',['set_vec_exp',['../classSolutions.html#a2ed5e375ab73d2eb17b7e317f657bcb0',1,'Solutions']]],
  ['set_5fvec_5ff_17',['set_vec_F',['../classSolutions.html#aa1e230a264f9c0d60f009446cf183e61',1,'Solutions']]],
  ['solutions_18',['Solutions',['../classSolutions.html',1,'Solutions'],['../classSolutions.html#a7e09debc04602a97490e37e3d917969c',1,'Solutions::Solutions()']]],
  ['solutions_2ecpp_19',['solutions.cpp',['../solutions_8cpp.html',1,'']]],
  ['solutions_2eh_20',['solutions.h',['../solutions_8h.html',1,'']]]
];
