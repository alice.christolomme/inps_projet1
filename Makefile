CC=g++
CFLAGS= -Wall -Wextra -std=c++11
LIBS = -larmadillo
TARGET = bin/exec
OBJS = obj/main.o obj/solutions.o obj/globals.o
CXXTEST = cxxtestgen
CXXTESTFLAGS = --error-printer
TARGET_TEST = bin/test
TESTS = tests/test_solutions.h
OBJS_TEST = tests/tests.o obj/solutions.o obj/globals.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(LDFLAGS) $^ -o $@ $(LIBS)

obj/%.o: src/%.cpp headers/%.h
	$(CXX) $(CXXFLAGS) -c $< -o $@

obj/main.o: src/main.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@


#Tests

test: $(TARGET_TEST)
	rm -f bin/test.cpp

$(TARGET_TEST): $(OBJS_TEST)
	$(CC) $^ -o $@ $(LIBS) 

tests/test.o: bin/test.cpp
	$(CXX) $(CFLAGS) $< -o $@

tests/%.cpp: $(TESTS)
	cxxtestgen --error-printer -o $@ $<





.PHONY: clean all
clean:
	rm -f ./obj/*.o
	rm -f ./filesResult/*.csv
	rm -f ./tests/*.o