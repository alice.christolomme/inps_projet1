#ifndef GLOBALS_H
#define GLOBALS_H
// general todo : this is a .h file there i no need for the 'extern' here

namespace constants
{
    extern int nb_level; //number of level of energy
    extern int nb_points; //number of points in the evaluation


    /* --------------------------- Constants for the easy mode --------------------------- */

    /**
     * For the easy mode, all constants are set to 1
     * 
     * hbar equals to h/(2*pi) with h the Plank's constant
    */
    extern double hbar_easy;

    extern double w_easy;

    extern double m_easy;



    /* --------------------------- Constants for the hard mode --------------------------- */

    /**
     *  h is Plank's constant
     * hbar equals to h/(2*pi)
    */
    extern double h;
    extern double hbar;

    /**
     * c is the celery of light
    */
    extern double c;

    /**
     * omega
    */
    extern double w_n;
    extern double w_e;

    /**
     * The quantum oscillator equation use a mass, of a neutron or an electron, depending of the system
     * 
     * m_e is the mass of an electron, in Mev/c^2
     * m_n is the mass of a neutron, in Mev/c^2
    */
    extern double m_e;
    extern double m_n;
};



#endif