/**
*@file test_solutions.h 
unit test file of solutions.cpp
*/

#ifndef TEST_SOLUTIONS
#define TEST_SOLUTIONS

#include <iostream>
#include <cxxtest/TestSuite.h>
#include "../headers/solutions.h"

class TestSolutions : public CxxTest::TestSuite
{
    public:
    void testConstructor()
    {
        TS_TRACE("Starting test for the Constructor");
        Solutions M = Solutions(10,200);
        arma::mat m1;
        m1.set_size(10,200);
        TS_ASSERT(arma::size(M.psi_matrix) == arma::size(m1));
        TS_ASSERT(arma::size(M.H) == arma::size(m1));
        arma::rowvec v1;
        v1.set_size(200);
        TS_ASSERT(arma::size(M.E) == arma::size(v1));
        arma::colvec v2;
        v2.set_size(10);
        TS_ASSERT(arma::size(M.F) == arma::size(v2));
        TS_ASSERT_EQUALS(M.F(0),1);
        arma::rowvec v3;
        v3.arma::rowvec::ones(200);
        double n= arma::norm(M.H.row(0)-v3,"inf");
        TS_ASSERT_DELTA(n, 0., 1e-7);

        TS_TRACE("Finishing test for the Constructor\n");
    }

    void testFact(void)
    {
        TS_TRACE("Starting tests for the factorial method");
        Solutions M;
        TS_ASSERT_EQUALS(M.Solutions::fact(5), 120);
        TS_ASSERT_EQUALS(M.Solutions::fact(0), 1);
        TS_TRACE("Finishing test for the factorial method\n");
    }

    void testMesh(void)
    {
        TS_TRACE("Starting tests for the mesh method");
        Solutions M;
        arma::rowvec z = M.Solutions::set_mesh(-5,5,21);
        arma::rowvec expected_mesh = {-5,-4.5,-4,-3.5,-3,-2.5,-2,-1.5,-1,-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5};
        double n = arma::norm(z-expected_mesh,"inf");
        TS_ASSERT_DELTA(n, 0., 1e-9);
        arma::rowvec z2 = M.Solutions::set_mesh(-2.3,6.1,2);
        arma::rowvec expected_mesh2 = {-2.3,6.1};
        n = arma::norm(z2-expected_mesh2,"inf");
        TS_ASSERT_DELTA(n, 0., 1e-9);
        TS_TRACE("Finishing test for the mesh method\n");

    }

    void testHermit()
    {
        TS_TRACE("Starting tests for the hermit method");


        //We test the hermite method for 11 points between -10 and 10
        arma::rowvec z = arma::linspace(-10., 10., 11).arma::mat::t();
        Solutions M;
        M.H.ones(8, 11);
        M.set_hermit(z);
        arma::mat T;
        T.set_size(8,11);
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                T(i,j)=std::hermite(i,z(j));
            }
        }
        double n= arma::norm(M.H-T,"inf");
        TS_ASSERT_DELTA(n, 0., 1e-7);
        TS_TRACE("Finishing test for the hermit method\n");
    }

    void testExp()
    {
        TS_TRACE("Starting tests for the set_vec_exp method");
        arma::rowvec z = arma::linspace(-10., 10., 11).arma::mat::t();
        arma::rowvec true_exp={1.9287498479639178e-22, 1.2664165549094176e-14, 1.522997974471263e-08, 0.00033546262790251185, 0.1353352832366127, 1.0, 0.1353352832366127, 0.00033546262790251185, 1.522997974471263e-08, 1.2664165549094176e-14, 1.9287498479639178e-22};
        /*obtain on python with :
        z=numpy.linspace(-10,10,11)
        for i in range (0,11) :
            print (math.exp(-z[i]*z[i]/2))*/
        Solutions M;
        M.set_vec_exp(1,1,1,z);
        double n= arma::norm(M.E-true_exp,"inf");
        TS_TRACE("set_vec_exp test is for easy mode");
        TS_ASSERT_DELTA(n, 0., 1e-7);
        TS_TRACE("Finishing test for the set_vec_exp\n");
    }

    void testF()
    {
        TS_TRACE("Starting tests for the set_F method");
        Solutions M;
        M.F.arma::colvec::ones(10);
        M.set_vec_F();
        arma::colvec T={1, 2, 8, 48, 384, 3840, 46080, 645120, 10321920, 185794560}; //vector of i!*2^i for i between 0 and 9
        T=1/sqrt(T);
        double n= arma::norm(M.F-T,"inf");
        TS_ASSERT_DELTA(n, 0., 1e-7);
        TS_TRACE("Finishing test for the set_F method\n ");
    }

    void test_scal()
    {
        TS_TRACE("Starting tests for the set_scal method");
        arma::rowvec points={-7.25385182, -6.54165545, -5.94807118, -5.414929  , -4.92002852,-4.45191115, -4.00367161, -3.57072198, -3.14979668, -2.73844582, -2.33475115, -1.93715458, -1.54434826, -1.1552002 , -0.76870138, -0.38392601,  0.        ,  0.38392601,  0.76870138,  1.1552002 , 1.54434826,  1.93715458,  2.33475115,  2.73844582,  3.14979668, 3.57072198,  4.00367161,  4.45191115,  4.92002852,  5.414929  , 5.94807118,  6.54165545,  7.25385182};
        arma::rowvec weights={1.15331622e-23, 1.65709474e-19, 2.40778568e-16, 9.43481416e-14, 1.47398094e-11, 1.12892225e-09, 4.80774568e-08, 1.23769337e-06, 2.04236841e-05, 2.25442771e-04, 1.71845464e-03, 9.26568997e-03, 3.59879823e-02, 1.02069080e-01, 2.13493931e-01, 3.31552001e-01, 3.83785267e-01, 3.31552001e-01, 2.13493931e-01, 1.02069080e-01, 3.59879823e-02, 9.26568997e-03, 1.71845464e-03, 2.25442771e-04, 2.04236841e-05, 1.23769337e-06, 4.80774568e-08, 1.12892225e-09, 1.47398094e-11, 9.43481416e-14, 2.40778568e-16, 1.65709474e-19, 1.15331622e-23};
        Solutions N= Solutions(8, 33);
        N.set_scal(points,weights);
        arma::mat Id;
        Id.eye(8,8);
        double n= arma::norm(N.scal-Id,"inf");
        TS_ASSERT_DELTA(n, 0., 1e-7);
        TS_TRACE("The orthonormality of the solutions has been verified");
        TS_TRACE("Finishing test for the set_scal method");
    }
};

#endif